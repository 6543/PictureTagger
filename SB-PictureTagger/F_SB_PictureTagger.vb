﻿Option Explicit On

Public Class SB_PictureTagger

    '###################################################################################################################
    '############### Main ##############################################################################################
    '###################################################################################################################

    '###########################
    '###  Declare Variables  ###
    '###########################

    Public Pub_sPath As String                  'Pfad des Aktuell Arbeits Verzeichnises
    Public Pub_aPfade(0) As String              'Array mit den Photo Pfaden
    Public Pub_nPfadeIndex As Long = 0          'IndexNR des aktuell Geladenen Photos aus pub_aPfade
    Public Pub_aConfigForm(0) As String         'Array mit FormConfiguration
    Public Pub_nOrdnerTiefe As Integer = 0      'Recursive Ordnertiefe beim Ordnereinlesen
    Public Pub_aSupportedFiles() As String = {".jpg", ".bmp", ".gif", ".png", ".jp2", ".tiff"}     'Speichert die Unterstützten Extentions

    Private nCountMarks As Integer


    '###################################################################################################################
    '####################### Private Subs ##############################################################################
    '###################################################################################################################

    Private Sub Clear()
        tb_Bild_Name.Clear()
        tb_Bild_Time.Clear()
        tb_Bild_Description.Clear()

        tb_Place_Name.Clear()
        tb_Place_Addresse.Clear()
        tb_Place_GPS.Clear()
        tb_Place_Description.Clear()

        tb_AktMark_Name.Clear()
        tb_AktMark_Description.Clear()


        'Entlehre Pfadangabe
        Label7.Text = ""

        'Entlehre TaggedIMG1
        TaggedIMG1.Clear()

        nCountMarks = 0
        TaggedIMG1.RO = True

        'GroubBox
        GB_Bild.Enabled = False
        GB_Ort.Enabled = False
        GB_Mark.Enabled = False

        'Aktiv mark TBs
        tb_AktMark_Description.Enabled = False
        tb_AktMark_Name.Enabled = False

    End Sub

    Private Sub SaveData()
        'Überprüfe ob Änderungen forgenommen #UND# Ob eine ID existiert #UND# Ob das Bild existiert
        If (TaggedIMG1.ID <> "" Or Nothing) And System.IO.File.Exists(Pub_aPfade(Pub_nPfadeIndex)) Then
            Data_Save()
        End If
    End Sub 'Save Taggs when Changes are hapened

    Private Sub Resize2()

    End Sub
    '###################################################################################################################
    '####################### EVENTS ####################################################################################
    '###################################################################################################################
#Region "Events"
    '################################################
    '############### Form Events ####################
    '################################################

    Private Sub SB_PictureTagger_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        SaveData()
    End Sub 'Form1_Closing

    Private Sub SB_PictureTagger_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Clear()
        cb_marks_visible.Checked = True
        TaggedIMG1.Marks_Visible = cb_marks_visible.Checked
    End Sub 'SB_PictureTagger_Load


    '################################################
    '############### Buton Events: Click ############
    '################################################

    Private Sub b_loaddir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles b_loaddir.Click
        'Laden eines zu Taggenden Bilder-Verzeichnis
        Dim sTMP As String
        sTMP = Tools.Dialoge.FolderBrowserDialog()
        If System.IO.Directory.Exists(sTMP) Then
            SaveData()
            Clear()
            Pub_sPath = sTMP
            LoadVerzeichnis()
            Data_Load()
        End If
    End Sub 'Öffne Ordner -> wenn Pfad OK -> Save Taggs -> Lade Verzeichnis -> Load Tagged IMG

    Private Sub b_reloaddir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles b_reloaddir.Click
        SaveData()
        Clear()
        LoadVerzeichnis()
        Data_Load()
    End Sub 'Save Taggs -> Lade Verzeichnis -> Load Tagged IMG

    Private Sub b_next_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles b_next.Click
        If Pub_nPfadeIndex < UBound(Pub_aPfade) Then
            SaveData()
            Clear()
            Pub_nPfadeIndex += 1
            Data_Load()
        End If
    End Sub 'Speichere Taggs -> Next Index -> Load Tagged IMG

    Private Sub b_last_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles b_last.Click
        If Pub_nPfadeIndex > 0 Then
            SaveData()
            Clear()
            Pub_nPfadeIndex = Pub_nPfadeIndex - 1
            Data_Load()
        End If
    End Sub 'Speichere Taggs -> Last Index -> Load Tagged IMG

    Private Sub b_exit_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles b_exit.Click
        Me.Close()
    End Sub 'b_exit_Click_1

    '################################################
    '###### CheckBox Events: CheckStateChanged ######
    '################################################

    Private Sub cb_marks_visible_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_marks_visible.CheckStateChanged
        TaggedIMG1.Marks_Visible = cb_marks_visible.Checked
    End Sub 'Visible/Hide Markierungen

    '###################################################################################################################
    '############### NumericUpDown Events: ValueChanged ################################################################
    '###################################################################################################################

    Private Sub NumericUpDown_OrdnerTiefe_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles NumericUpDown_OrdnerTiefe.ValueChanged
        SaveData()
        Pub_nOrdnerTiefe = NumericUpDown_OrdnerTiefe.Value
        LoadVerzeichnis()
        Data_Load()
    End Sub ''Speichere Taggs -> Neue Ordner Tiefe -> Lade Verzeichnis -> Load Tagged IMG


    '###################################################################################################################
    '############### TextBox Events: TextChanged #######################################################################
    '###################################################################################################################

    Private Sub tb_Bild_Description_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tb_Bild_Description.TextChanged
        TaggedIMG1.Description = tb_Bild_Description.Text
    End Sub

    Private Sub tb_Bild_Name_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tb_Bild_Name.TextChanged
        TaggedIMG1.Title = tb_Bild_Name.Text
    End Sub

    Private Sub tb_Bild_Time_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tb_Bild_Time.TextChanged
        TaggedIMG1.Time = tb_Bild_Time.Text
    End Sub

    Private Sub tb_Place_Addresse_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tb_Place_Addresse.TextChanged
        TaggedIMG1.Place.Adress = tb_Place_Addresse.Text
    End Sub

    Private Sub tb_Place_Description_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tb_Place_Description.TextChanged
        TaggedIMG1.Place.Description = tb_Place_Description.Text
    End Sub

    Private Sub tb_Place_GPS_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tb_Place_GPS.TextChanged
        TaggedIMG1.Place.GPS = tb_Place_GPS.Text
    End Sub

    Private Sub tb_Place_Name_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tb_Place_Name.TextChanged
        TaggedIMG1.Place.Title = tb_Place_Name.Text
    End Sub

    Private Sub tb_AktMark_Description_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tb_AktMark_Description.TextChanged
        TaggedIMG1.ActivMarkDescription = tb_AktMark_Description.Text
    End Sub 'tb_AktMark_Description_TextChanged

    Private Sub tb_AktMark_Name_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tb_AktMark_Name.TextChanged
        TaggedIMG1.ActivMarkName = tb_AktMark_Name.Text
    End Sub 'tb_AktMark_Name_TextChanged


    '###################################################################################################################
    '############### TAggedIMG Events ##################################################################################
    '###################################################################################################################

    Private Sub TaggedIMG1_LikeToCreatMark(ByVal Left As Integer, ByVal Top As Integer, ByVal Width As Integer, ByVal Height As Integer) Handles TaggedIMG1.LikeToCreatMark
        If TaggedIMG1.Marks_Create(nCountMarks, Left, Top, Width, Height) Then nCountMarks += 1
    End Sub 'TaggedIMG1_LikeToCreatMark

    Private Sub TaggedIMG1_ActivMarkChanged(ByVal arg As Boolean) Handles TaggedIMG1.ActivMarkChanged
        If TaggedIMG1.ActivMarkID <> Nothing Or TaggedIMG1.ActivMarkID <> "" Then
            tb_AktMark_Name.Text = TaggedIMG1.ActivMarkName
            tb_AktMark_Name.Enabled = True
            tb_AktMark_Description.Text = TaggedIMG1.ActivMarkDescription
            tb_AktMark_Description.Enabled = True
        Else
            tb_AktMark_Name.Text = ""
            tb_AktMark_Name.Enabled = False
            tb_AktMark_Description.Text = ""
            tb_AktMark_Description.Enabled = False
        End If
    End Sub 'TaggedIMG1_ActivMarkChanged


#End Region 'Events


    '###################################################################################################################
    '############### Lade ##############################################################################################
    '###################################################################################################################

    Public Sub LoadVerzeichnis()
        If System.IO.Directory.Exists(Pub_sPath) Then
            Pub_aPfade = Tools.Filesystem.GetFilesFromDir(Pub_sPath, Pub_aSupportedFiles, Pub_nOrdnerTiefe)
            Pub_nPfadeIndex = 0
        End If
    End Sub 'Lade Bilder in pub_aPfade Array; Setze Index auf 0


    '###################################################################################################################
    '###################### D A T A ####################################################################################
    '###################################################################################################################

    'Taggs auslesen (jezt noch aus INI - später aus DB)
    Private Sub Data_Load()
        ' ### Settings ###
        Dim InIForEachDat As Boolean = False

        'Textvelder Lehren
        Me.Clear()

        'Absicherung für Undimensioniertes Array
        If Pub_aPfade.Length = 0 Then ReDim Pub_aPfade(0)

        'Bereinige Falsche Indexangaben
        If UBound(Pub_aPfade) < Pub_nPfadeIndex Then Pub_nPfadeIndex = UBound(Pub_aPfade)

        ' ####################
        'Deklarieren
        'Dim
        Dim INI_Locate As String
        Dim INI_Name As String
        Dim PicturePath As String
        Dim sKey As String 'Um INI anzusteuern

        PicturePath = Pub_aPfade(Pub_nPfadeIndex)

        'Bildpfad auf existenz Prüfen
        If System.IO.File.Exists(PicturePath) Then
            ' ### Bestimmen Der INI ###
            If InIForEachDat Then
                INI_Name = System.IO.Path.Combine(System.IO.Path.GetFileName(PicturePath), ".ini")
                INI_Locate = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(PicturePath), INI_Name)
            Else
                INI_Name = "Tagg.ini"
                INI_Locate = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(PicturePath), INI_Name)
            End If

            'TaggedIMG
            TaggedIMG1.Image = Image.FromFile(PicturePath)
            TaggedIMG1.ID = System.IO.Path.GetFileName(PicturePath) 'Lade Wichtigste Eigenschaften = Immer Forhanden bei Geladenem TaggedIMG
            TaggedIMG1.RO = False

            GB_Bild.Enabled = True
            GB_Ort.Enabled = True
            GB_Mark.Enabled = True

            TaggedIMG1.Marks_Visible = cb_marks_visible.Checked


            '####################### Read from INI ######################################
            If System.IO.File.Exists(INI_Locate) Then
                sKey = TaggedIMG1.ID

                'Lade Optionalen Rest:
                'Titel
                TaggedIMG1.Title = Tools.Data.File.INI_ReadValue(INI_Locate, sKey, "Title", Nothing)
                'Kurzbeschreibung
                '##############################################################
                'ReFormate Description Text from "Description"
                TaggedIMG1.Description = Tools.Convert.Coding.Base64_To_Ascii(Tools.Data.File.INI_ReadValue(INI_Locate, sKey, "Description", Nothing))
                '##############################################################

                'Zeitangabe
                TaggedIMG1.Time = Tools.Data.File.INI_ReadValue(INI_Locate, sKey, "Time", Nothing)
                'Ortsangabe-Title
                TaggedIMG1.Place.Title = Tools.Data.File.INI_ReadValue(INI_Locate, sKey, "Place_Title", Nothing)
                'Ortsangabe-Adresse
                TaggedIMG1.Place.Adress = Tools.Data.File.INI_ReadValue(INI_Locate, sKey, "Place_Adresse", Nothing)
                'Ortsangabe-Kurzbeschreibung
                '##############################################################
                'ReFormate Description Text from "Place.Description"
                TaggedIMG1.Place.Description = Tools.Convert.Coding.Base64_To_Ascii(Tools.Data.File.INI_ReadValue(INI_Locate, sKey, "Place_Description", Nothing))
                '##############################################################

                'Ortsangabe-GPS Koordinaten
                TaggedIMG1.Place.GPS = Tools.Data.File.INI_ReadValue(INI_Locate, sKey, "Place_GPS", Nothing)


                'Führ Makrierungen einlese Schleife
                ''Dim i As Long
                Dim Mark_ID As String
                Dim Mark_Left As Integer
                Dim Mark_Top As Integer
                Dim Mark_Width As Integer
                Dim Mark_Height As Integer
                Dim Mark_Description As String
                Dim Mark_Name As String
                Dim Mark_Index As Integer

                'Load Marks
                Mark_Index = Convert.ToInt32(Tools.Data.File.INI_ReadValue(INI_Locate, sKey, "Mark_MaxIndex", "-1"))
                If Mark_Index >= 0 Then
                    For counter = 0 To Mark_Index
                        Mark_ID = counter
                        Mark_Left = Convert.ToInt32(Tools.Data.File.INI_ReadValue(INI_Locate, sKey, "Mark" & Convert.ToString(counter) & "_Left"))
                        Mark_Top = Convert.ToInt32(Tools.Data.File.INI_ReadValue(INI_Locate, sKey, "Mark" & Convert.ToString(counter) & "_Top"))
                        Mark_Width = Convert.ToInt32(Tools.Data.File.INI_ReadValue(INI_Locate, sKey, "Mark" & Convert.ToString(counter) & "_Width"))
                        Mark_Height = Convert.ToInt32(Tools.Data.File.INI_ReadValue(INI_Locate, sKey, "Mark" & Convert.ToString(counter) & "_Height"))
                        Mark_Name = Tools.Data.File.INI_ReadValue(INI_Locate, sKey, "Mark" & Convert.ToString(counter) & "_Name")
                        Mark_Description = Tools.Convert.Coding.Base64_To_Ascii(Tools.Data.File.INI_ReadValue(INI_Locate, sKey, "Mark" & Convert.ToString(counter) & "_Description"))

                        TaggedIMG1.Marks_Set(Mark_ID, "Create", Mark_Left, Mark_Top, Mark_Width, Mark_Height, Mark_Name, Mark_Description)

                        Mark_ID = 0
                        Mark_Left = 0
                        Mark_Top = 0
                        Mark_Width = 0
                        Mark_Height = 0
                        Mark_Name = ""
                        Mark_Description = ""

                    Next
                    nCountMarks = Mark_Index + 1
                End If

            End If
            '######################## End Read #########################################



            'Zeigt den BildPfad an
            Label7.Text = Pub_aPfade(Pub_nPfadeIndex)

            'Befülle die TextBox-en
            tb_Bild_Name.Text = TaggedIMG1.Title
            tb_Bild_Time.Text = TaggedIMG1.Time
            tb_Bild_Description.Text = TaggedIMG1.Description

            tb_Place_Name.Text = TaggedIMG1.Place.Title
            tb_Place_Addresse.Text = TaggedIMG1.Place.Adress
            tb_Place_GPS.Text = TaggedIMG1.Place.GPS
            tb_Place_Description.Text = TaggedIMG1.Place.Description
        End If




    End Sub

    Private Sub Data_Save()

        '### INI Einstellungen ###
        Dim ForEachDat As Boolean = False
        Dim INI_Locate As String
        Dim INI_Name As String
        Dim PicturePath As String = Pub_aPfade(Pub_nPfadeIndex)

        If ForEachDat Then
            INI_Name = System.IO.Path.Combine(System.IO.Path.GetFileName(PicturePath), ".ini")
            INI_Locate = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(PicturePath), INI_Name)
        Else
            INI_Name = "Tagg.ini"
            INI_Locate = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(PicturePath), INI_Name)
        End If

        '### Main #################
        If System.IO.File.Exists(PicturePath) Then
            If Not System.IO.File.Exists(INI_Locate) Then System.IO.File.Create(INI_Locate)

            'Deklarationen
            Dim sKey = TaggedIMG1.ID

            'Titel
            Tools.Data.File.INI_WriteValue(INI_Locate, sKey, "Title", TaggedIMG1.Title)

            'Kurzbeschreibung
            '##############################################################
            'Formate Description Text from "Description"
            Tools.Data.File.INI_WriteValue(INI_Locate, sKey, "Description", Tools.Convert.Coding.Ascii_To_Base64(TaggedIMG1.Description))
            '##############################################################

            'Zeitangabe
            Tools.Data.File.INI_WriteValue(INI_Locate, sKey, "Time", TaggedIMG1.Time)
            'Ortsangabe-Title
            Tools.Data.File.INI_WriteValue(INI_Locate, sKey, "Place_Title", TaggedIMG1.Place.Title)
            'Ortsangabe-Adresse
            Tools.Data.File.INI_WriteValue(INI_Locate, sKey, "Place_Adresse", TaggedIMG1.Place.Adress)

            'Ortsangabe-Kurzbeschreibung
            '##############################################################
            'Formate Description Text from "TMP_TaggIMG.Place.Description"
            Tools.Data.File.INI_WriteValue(INI_Locate, sKey, "Place_Description", Tools.Convert.Coding.Ascii_To_Base64(TaggedIMG1.Place.Description))
            '##############################################################

            'Ortsangabe-GPS Koordinaten
            Tools.Data.File.INI_WriteValue(INI_Locate, sKey, "Place_GPS", TaggedIMG1.Place.GPS)


            Dim oTemp() As Mark = TaggedIMG1.Marks_GetAll
            Dim counter As Integer
            

            counter = -1
            For Each Markierung As Mark In oTemp
                If (Markierung.Command = "Nothing") Or (Markierung.Command = "Change") Or (Markierung.Command = "Create") Then
                    counter += 1

                    Tools.Data.File.INI_WriteValue(INI_Locate, sKey, "Mark" & Convert.ToString(counter) & "_ID", Convert.ToString(counter))
                    Tools.Data.File.INI_WriteValue(INI_Locate, sKey, "Mark" & Convert.ToString(counter) & "_Left", Convert.ToString(Markierung.Left))
                    Tools.Data.File.INI_WriteValue(INI_Locate, sKey, "Mark" & Convert.ToString(counter) & "_Top", Convert.ToString(Markierung.Top))
                    Tools.Data.File.INI_WriteValue(INI_Locate, sKey, "Mark" & Convert.ToString(counter) & "_Width", Convert.ToString(Markierung.Width))
                    Tools.Data.File.INI_WriteValue(INI_Locate, sKey, "Mark" & Convert.ToString(counter) & "_Height", Convert.ToString(Markierung.Height))
                    Tools.Data.File.INI_WriteValue(INI_Locate, sKey, "Mark" & Convert.ToString(counter) & "_Name", Convert.ToString(Markierung.Name))
                    Tools.Data.File.INI_WriteValue(INI_Locate, sKey, "Mark" & Convert.ToString(counter) & "_Description", Tools.Convert.Coding.Ascii_To_Base64(Markierung.Description))

                End If

            Next
            Tools.Data.File.INI_WriteValue(INI_Locate, sKey, "Mark_MaxIndex", counter)

        End If
    End Sub

    
End Class
