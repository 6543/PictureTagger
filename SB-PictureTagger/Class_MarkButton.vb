﻿
Public Class MarkButton
    'Erbe von Class Button
    Inherits Windows.Forms.Button

    '########################################################################################
    '##############################  Declarations  ##########################################
    '########################################################################################

    'Variablen
    Private nIndex As Integer
    Private components As System.ComponentModel.IContainer
    Private bRO

    'Objecte
    Friend WithEvents Menu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents Menu_Move As System.Windows.Forms.ToolStripMenuItem           'Verschiebe Markierung
    Friend WithEvents Menu_SiceChange As System.Windows.Forms.ToolStripMenuItem     'Ändere Größe
    Friend WithEvents Menu_Delete As System.Windows.Forms.ToolStripMenuItem         'Verschiebe Markierung
    Friend WithEvents Menu_Hide As System.Windows.Forms.ToolStripMenuItem           'Verstecke einzelne Markierung

    'Events
    Public Event LikeMove(ByVal sender As Object)
    Public Event LikeSiceChange(ByVal sender As Object)
    Public Event LikeDelete(ByVal sender As Object)


    '########################################################################################
    '##############################  Property  ##############################################
    '########################################################################################

    Public Property Position() As Rectangle
        Get
            Return New Rectangle(Me.Left, Me.Top, Me.Width, Me.Height)
        End Get
        Set(ByVal value As Rectangle)
            Me.Width = value.Width
            Me.Height = value.Height
            Me.Left = value.Left
            Me.Top = value.Top
        End Set
    End Property 'PositionOnTaggedIMG

    Private bDeleted As Boolean
    Public Property Deleted As Boolean
        Get
            Return Me.bDeleted
        End Get
        Set(ByVal value As Boolean)
            If value Then Me.Clear()
            Me.bDeleted = value
        End Set
    End Property

    Public ReadOnly Property Index As Integer
        Get
            Return nIndex
        End Get
    End Property 'Index

    '########################################################################################
    '##############################  PUBLIC Subs  ###########################################
    '########################################################################################

    Public Sub Clear()
        With Me
            .Visible = False
            .Width = 0
            .Height = 0
            .Left = 0
            .Top = 0
            .Text = ""
            .FlatAppearance.BorderSize = 3
        End With
    End Sub 'Clear

    Public Sub New(Optional ByVal Index As Integer = -1, Optional ByVal RO As Boolean = False)
        '
        'Main
        '
        Me.FlatStyle = Windows.Forms.FlatStyle.Flat
        Me.BackColor = Drawing.Color.Transparent
        Me.BackgroundImage = Nothing
        Me.ForeColor = Drawing.Color.Transparent
        Me.FlatAppearance.BorderColor = Drawing.Color.Black
        Me.FlatAppearance.MouseDownBackColor = Drawing.Color.Transparent
        Me.FlatAppearance.MouseOverBackColor = Drawing.Color.Transparent
        Me.TabStop = False

        Me.Menu = New System.Windows.Forms.ContextMenuStrip
        Me.Menu_Move = New System.Windows.Forms.ToolStripMenuItem
        Me.Menu_SiceChange = New System.Windows.Forms.ToolStripMenuItem
        Me.Menu_Hide = New System.Windows.Forms.ToolStripMenuItem
        Me.Menu_Delete = New System.Windows.Forms.ToolStripMenuItem

        Me.nIndex = Index
        Me.bRO = RO

        '
        'Menu
        '
        Me.Menu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Move, Me.Menu_SiceChange, Me.Menu_Hide, Me.Menu_Delete})
        Me.Menu.Name = "Menu"
        Me.Menu.Size = New System.Drawing.Size(202, 70)

        '
        'Menu_Move
        '
        Me.Menu_Move.Name = "Menu_Move"
        Me.Menu_Move.Size = New System.Drawing.Size(201, 22)
        Me.Menu_Move.Text = "Verschiebe Markierung"
        Me.Menu_Move.Enabled = Not RO 'Schreibschutz setzen wenn übergenben
        Me.Menu_Move.Enabled = False '<<#<<#<<#<<#<<#<<#<<#<<#<<#<<#<<#<<#<<#<<#<<#<<#<<#<<#<<#<<#<<#<<#<<#<< Noch in Arbeit >>>>>>>>

        '
        'Menu_SiceChange
        '
        Me.Menu_SiceChange.Name = "Menu_SiceChange"
        Me.Menu_SiceChange.Size = New System.Drawing.Size(201, 22)
        Me.Menu_SiceChange.Text = "Ändere Größe"
        Me.Menu_SiceChange.Enabled = Not RO 'Schreibschutz setzen wenn übergenben

        '
        'Menu_Hide
        '
        Me.Menu_Hide.Name = "Menu_Hide"
        Me.Menu_Hide.Size = New System.Drawing.Size(201, 22)
        Me.Menu_Hide.Text = "Verstecke Markierung"
        Me.Menu_Hide.Enabled = Not RO 'Schreibschutz setzen wenn übergenben

        '
        'Menu_Delete
        '
        Me.Menu_Delete.Name = "Menu_Delete"
        Me.Menu_Delete.Size = New System.Drawing.Size(201, 22)
        Me.Menu_Delete.Text = "Lösche Markierung"
        Me.Menu_Delete.Enabled = Not RO 'Schreibschutz setzen wenn übergenben

        If nIndex >= 0 Then Me.ContextMenuStrip = Me.Menu 'Wenn Gültiget Index Aktiviere Menü

        Me.Clear()
    End Sub ' New(-1, False) will execute wenn declare a new instance


    '########################################################################################
    '##############################  Sub on Event  ##########################################
    '########################################################################################

    'Private Sub MarkButtnon_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus
    '    Me.FlatAppearance.BorderSize = 4
    'End Sub 'MarkButnon_GotFocus Event

    'Private Sub MarkButtnon_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LostFocus
    '    Me.FlatAppearance.BorderSize = 3
    'End Sub 'MarkButnon_LostFocus Event

    Private Sub Menu_Delete_Click() Handles Menu_Delete.Click
        RaiseEvent LikeDelete(Me)
    End Sub 'Menu_Delete_Click = Me.LikeDelete

    Private Sub Menu_Hide_Click() Handles Menu_Hide.Click
        Me.Visible = False
    End Sub 'Menu_Hide_Click = Me.Visible <- False

    Private Sub Menu_Move_Click() Handles Menu_Move.Click
        RaiseEvent LikeMove(Me)
    End Sub 'Menu_Move_Click = Me.LikeMove

    Private Sub Menu_SiceChange_Click() Handles Menu_SiceChange.Click
        RaiseEvent LikeSiceChange(Me)
    End Sub 'Menu_SiceChange_Click = Me.LikeSiceChange


End Class 'MarkButton
