﻿
Public Class Place

    '########################################################################################
    '##############################  Declare Events  ########################################
    '########################################################################################

    Public Event PropertyTitleChanged()
    Public Event PropertyAdressChanged()
    Public Event PropertyDescriptionChanged()
    Public Event PropertyGPSChanged()


    '########################################################################################
    '##############################  Property  ##############################################
    '########################################################################################

    Private sTitle As String
    Public Property Title() As String
        Get
            Return sTitle
        End Get
        Set(ByVal value As String)
            If Not value = sTitle Then
                sTitle = value
                RaiseEvent PropertyTitleChanged()
            End If
        End Set
    End Property

    Private sAdress As String
    Public Property Adress() As String
        Get
            Return sAdress
        End Get
        Set(ByVal value As String)
            If Not value = sAdress Then
                sAdress = value
                RaiseEvent PropertyAdressChanged()
            End If
        End Set
    End Property

    Private sDescription As String
    Public Property Description() As String
        Get
            Return sDescription
        End Get
        Set(ByVal value As String)
            If Not value = sDescription Then
                sDescription = value
                RaiseEvent PropertyDescriptionChanged()
            End If
        End Set
    End Property

    Private sGPS As String
    Public Property GPS() As String
        Get
            Return sGPS
        End Get
        Set(ByVal value As String) 'Später eventuelle überprüfung auf format und deren Umwandlung -GPS Formate-
            If Not value = sGPS Then
                sGPS = value
                RaiseEvent PropertyGPSChanged()
            End If
        End Set
    End Property


    '########################################################################################
    '##############################  PUBLIC Subs  ###########################################
    '########################################################################################

    Public Sub Clear()
        Me.sAdress = Nothing
        Me.sDescription = Nothing
        Me.sGPS = Nothing
        Me.sTitle = Nothing
    End Sub 'Lehren des Objects


    '########################################################################################
    '##############################  Public Overrides  ######################################
    '########################################################################################

    Public Overrides Function GetHashCode() As Integer
        Dim sb As New System.Text.StringBuilder
        sb.Append(Me.sAdress)
        sb.Append(Me.sDescription)
        sb.Append(Me.sGPS)
        sb.Append(Me.sTitle)
        Return sb.ToString.GetHashCode()
    End Function 'GetHashCode


End Class 'Class Place