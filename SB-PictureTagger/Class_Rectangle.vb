﻿
Public Class Rectangle
    '########################################################################################
    '##############################  Private Declarations  ##################################
    '########################################################################################

    Private nLeft As Integer = 0
    Private nTop As Integer = 0
    Private nWidth As Integer = 0
    Private nHeight As Integer = 0


    '########################################################################################
    '##############################  Property  ##############################################
    '########################################################################################

    Public Property Left() As Integer
        Get
            Return Me.nLeft
        End Get
        Set(ByVal value As Integer)
            If value < 0 Then value = 0
            Me.nLeft = value
        End Set
    End Property 'Left

    Public Property Top() As Integer
        Get
            Return Me.nTop
        End Get
        Set(ByVal value As Integer)
            If value < 0 Then value = 0
            Me.nTop = value
        End Set
    End Property 'Top

    Public Property Width() As Integer
        Get
            Return Me.nWidth
        End Get
        Set(ByVal value As Integer)
            If value < 0 Then value = 0
            Me.nWidth = value
        End Set
    End Property 'Width

    Public Property Height() As Integer
        Get
            Return Me.nHeight
        End Get
        Set(ByVal value As Integer)
            If value < 0 Then value = 0
            Me.nHeight = value
        End Set
    End Property 'Height

    Public ReadOnly Property PointA As Drawing.Point
        Get
            Return New Drawing.Point(nLeft, nTop)
        End Get
    End Property 'RO PointA

    Public ReadOnly Property PointB As Drawing.Point
        Get
            Return New Drawing.Point(nLeft + nWidth, nTop + nHeight)
        End Get
    End Property 'RO PointB


    '########################################################################################
    '##############################  PUBLIC Functions  ######################################
    '########################################################################################


    Public Function AsString() As String
        With Me
            If .nLeft = Nothing Then nLeft = 0
            If .nTop = Nothing Then nTop = 0
            If .nWidth = Nothing Then nWidth = 0
            If .nHeight = Nothing Then nHeight = 0
            AsString = "Left: " & Convert.ToString(.nLeft) & "; Top: " & Convert.ToString(.nTop) & "; Width: " & Convert.ToString(.nWidth) & "; Height: " & Convert.ToString(.nHeight)
        End With
    End Function 'AsString


    '########################################################################################
    '##############################  PUBLIC Subs  ###########################################
    '########################################################################################

    'New(Left, Top, Width, Height)
    Public Sub New(Optional ByVal Left As Integer = Nothing, Optional ByVal Top As Integer = Nothing, Optional ByVal Width As Integer = Nothing, Optional ByVal Height As Integer = Nothing)
        With Me
            .Left = Left
            .Top = Top
            .Width = Width
            .Height = Height
        End With
    End Sub 'New

    Public Sub Clear()
        With Me
            .Left = 0
            .Top = 0
            .Width = 0
            .Height = 0
        End With
    End Sub

    '########################################################################################
    '##############################  PUBLIC Shared Functions  ###############################
    '########################################################################################

    Public Shared Function Points_to_Rectangle(ByVal PointA As System.Drawing.Point, ByVal PointB As System.Drawing.Point) As Rectangle
        Dim rRectangle As New Rectangle
        Dim A_x As Integer = PointA.X
        Dim A_y As Integer = PointA.Y
        Dim B_x As Integer = PointB.X
        Dim B_y As Integer = PointB.Y

        If A_x < B_x Then
            If A_y < B_y Then
                PointA.X = A_x
                PointA.Y = A_y
                PointB.X = B_x
                PointB.Y = B_y
            ElseIf A_y > B_y Then
                PointA.X = A_x
                PointA.Y = B_y
                PointB.X = B_x
                PointB.Y = A_y
            End If

        ElseIf A_x > B_x Then
            If A_y < B_y Then
                PointA.X = B_x
                PointA.Y = A_y
                PointB.X = A_x
                PointB.Y = B_y
            ElseIf A_y > B_y Then
                PointA.X = B_x
                PointA.Y = B_y
                PointB.X = A_x
                PointB.Y = A_y
            End If
        End If
        rRectangle.Left = PointA.X
        rRectangle.Top = PointA.Y
        rRectangle.Width = PointB.X - PointA.X
        rRectangle.Height = PointB.Y - PointA.Y

        Points_to_Rectangle = rRectangle

    End Function 'Points_to_Rectangle


    '########################################################################################
    '##############################  Public Overrides  ######################################
    '########################################################################################

    Public Overrides Function GetHashCode() As Integer
        Dim sb As New System.Text.StringBuilder
        sb.Append(Convert.ToString(Me.nHeight))
        sb.Append(Convert.ToString(Me.nLeft))
        sb.Append(Convert.ToString(Me.nTop))
        sb.Append(Convert.ToString(Me.nWidth))
        Return sb.ToString.GetHashCode()
    End Function 'GetHashCode


End Class 'Class Rectangle
