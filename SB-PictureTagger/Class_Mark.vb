﻿
Public Class Mark

    '########################################################################################
    '##############################  Private Declarations  ##################################
    '########################################################################################

    Private sID As String           '->  Das Kommando muss angegeben werden
    Private sCommand As String      '->  Das Kommando muss angegeben werden
    Private nLeft As Integer        '\
    Private nTop As Integer         ' \  Die Position muss angegeben werden. Der Datentyp Rectangle wird nur intern benutzt,
    Private nWidth As Integer       ' /  weshalb nach ausen die Eigenschaften einzeln angegeben werden müssen!
    Private nHeight As Integer       '/
    Private sName As String         '->  Der Name ist Optional
    Private sDescription As String  '->  Die Beschreibung ist Optional

    '########################################################################################
    '##############################  Property  ##############################################
    '########################################################################################

    Public Property ID As String
        Get
            Return Me.sID
        End Get
        Set(ByVal value As String)
            Me.sID = value
        End Set
    End Property 'ID

    Public Property Command As String
        Get
            Return Me.sCommand
        End Get
        Set(ByVal value As String)
            Me.sCommand = value
        End Set
    End Property 'Command

    Public Property Left() As Integer
        Get
            Return Me.nLeft
        End Get
        Set(ByVal value As Integer)
            If value >= 0 Then Me.nLeft = value
        End Set
    End Property 'Left

    Public Property Top() As Integer
        Get
            Return Me.nTop
        End Get
        Set(ByVal value As Integer)
            If value >= 0 Then Me.nTop = value
        End Set
    End Property 'Top

    Public Property Width() As Integer
        Get
            Return Me.nWidth
        End Get
        Set(ByVal value As Integer)
            If value >= 0 Then Me.nWidth = value
        End Set
    End Property 'Width

    Public Property Height() As Integer
        Get
            Return Me.nHeight
        End Get
        Set(ByVal value As Integer)
            If value >= 0 Then Me.nHeight = value
        End Set
    End Property 'Height

    Public Property Name As String
        Get
            Return Me.sName
        End Get
        Set(ByVal value As String)
            Me.sName = value
        End Set
    End Property 'Name

    Public Property Description As String
        Get
            Return Me.sDescription
        End Get
        Set(ByVal value As String)
            Me.sDescription = value
        End Set
    End Property 'Description


    '########################################################################################
    '##############################  PUBLIC Subs  ###########################################
    '########################################################################################

    Public Sub Clear()
        With Me
            .sID = ""
            .sCommand = ""
            .nLeft = -1
            .nTop = -1
            .nWidth = -1
            .nHeight = -1
            .sName = ""
            .sDescription = ""
        End With
    End Sub

    Public Sub New(Optional ByVal ID As String = "", Optional ByVal Command As String = "", Optional ByVal Left As Integer = -1, Optional ByVal Top As Integer = -1, Optional ByVal Width As Integer = -1, Optional ByVal Height As Integer = -1, Optional ByVal Name As String = "", Optional ByVal Description As String = "")
        With Me
            .sID = ID
            .sCommand = Command
            .nLeft = Left
            .nTop = Top
            .nWidth = Width
            .nHeight = Height
            .sName = Name
            .sDescription = Description
        End With
    End Sub

End Class 'Mark ### Export, Improt Vormat für Markierungen
