﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SB_PictureTagger
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Place1 As Place = New Place()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SB_PictureTagger))
        Me.Panel_Botom = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.b_reloaddir = New System.Windows.Forms.Button()
        Me.b_loaddir = New System.Windows.Forms.Button()
        Me.b_exit = New System.Windows.Forms.Button()
        Me.b_last = New System.Windows.Forms.Button()
        Me.b_next = New System.Windows.Forms.Button()
        Me.Panel_Right = New System.Windows.Forms.Panel()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.NumericUpDown_OrdnerTiefe = New System.Windows.Forms.NumericUpDown()
        Me.cb_marks_visible = New System.Windows.Forms.CheckBox()
        Me.GB_Ort = New System.Windows.Forms.GroupBox()
        Me.tb_Place_Description = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.tb_Place_Name = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.tb_Place_GPS = New System.Windows.Forms.TextBox()
        Me.tb_Place_Addresse = New System.Windows.Forms.TextBox()
        Me.GB_Mark = New System.Windows.Forms.GroupBox()
        Me.tb_AktMark_Description = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.tb_AktMark_Name = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.GB_Bild = New System.Windows.Forms.GroupBox()
        Me.tb_Bild_Description = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tb_Bild_Time = New System.Windows.Forms.TextBox()
        Me.tb_Bild_Name = New System.Windows.Forms.TextBox()
        Me.TaggedIMG1 = New TaggedIMG()
        Me.Panel_Botom.SuspendLayout()
        Me.Panel_Right.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.NumericUpDown_OrdnerTiefe, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GB_Ort.SuspendLayout()
        Me.GB_Mark.SuspendLayout()
        Me.GB_Bild.SuspendLayout()
        CType(Me.TaggedIMG1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel_Botom
        '
        Me.Panel_Botom.Controls.Add(Me.Label7)
        Me.Panel_Botom.Controls.Add(Me.b_reloaddir)
        Me.Panel_Botom.Controls.Add(Me.b_loaddir)
        Me.Panel_Botom.Controls.Add(Me.b_exit)
        Me.Panel_Botom.Controls.Add(Me.b_last)
        Me.Panel_Botom.Controls.Add(Me.b_next)
        Me.Panel_Botom.Location = New System.Drawing.Point(1, 636)
        Me.Panel_Botom.Name = "Panel_Botom"
        Me.Panel_Botom.Size = New System.Drawing.Size(1104, 87)
        Me.Panel_Botom.TabIndex = 12
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(11, 68)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(65, 13)
        Me.Label7.TabIndex = 7
        Me.Label7.Text = "Pfadangabe"
        '
        'b_reloaddir
        '
        Me.b_reloaddir.Font = New System.Drawing.Font("Comic Sans MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.b_reloaddir.Location = New System.Drawing.Point(420, 14)
        Me.b_reloaddir.Name = "b_reloaddir"
        Me.b_reloaddir.Size = New System.Drawing.Size(165, 51)
        Me.b_reloaddir.TabIndex = 11
        Me.b_reloaddir.Text = "Neu Laden"
        Me.b_reloaddir.UseVisualStyleBackColor = True
        '
        'b_loaddir
        '
        Me.b_loaddir.Font = New System.Drawing.Font("Comic Sans MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.b_loaddir.Location = New System.Drawing.Point(611, 14)
        Me.b_loaddir.Name = "b_loaddir"
        Me.b_loaddir.Size = New System.Drawing.Size(178, 51)
        Me.b_loaddir.TabIndex = 12
        Me.b_loaddir.Text = "Ordner Öffnen"
        Me.b_loaddir.UseVisualStyleBackColor = True
        '
        'b_exit
        '
        Me.b_exit.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.b_exit.Location = New System.Drawing.Point(915, 14)
        Me.b_exit.Name = "b_exit"
        Me.b_exit.Size = New System.Drawing.Size(178, 51)
        Me.b_exit.TabIndex = 13
        Me.b_exit.Text = "Beenden"
        Me.b_exit.UseVisualStyleBackColor = True
        '
        'b_last
        '
        Me.b_last.Font = New System.Drawing.Font("Comic Sans MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.b_last.Location = New System.Drawing.Point(11, 14)
        Me.b_last.Name = "b_last"
        Me.b_last.Size = New System.Drawing.Size(178, 51)
        Me.b_last.TabIndex = 8
        Me.b_last.Text = "Vorheriges Bild"
        Me.b_last.UseVisualStyleBackColor = True
        '
        'b_next
        '
        Me.b_next.Font = New System.Drawing.Font("Comic Sans MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.b_next.Location = New System.Drawing.Point(216, 14)
        Me.b_next.Name = "b_next"
        Me.b_next.Size = New System.Drawing.Size(178, 51)
        Me.b_next.TabIndex = 9
        Me.b_next.Text = "Nächstes Bild"
        Me.b_next.UseVisualStyleBackColor = True
        '
        'Panel_Right
        '
        Me.Panel_Right.Controls.Add(Me.GroupBox4)
        Me.Panel_Right.Controls.Add(Me.GB_Ort)
        Me.Panel_Right.Controls.Add(Me.GB_Mark)
        Me.Panel_Right.Controls.Add(Me.GB_Bild)
        Me.Panel_Right.Location = New System.Drawing.Point(735, 12)
        Me.Panel_Right.Name = "Panel_Right"
        Me.Panel_Right.Size = New System.Drawing.Size(370, 618)
        Me.Panel_Right.TabIndex = 13
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label8)
        Me.GroupBox4.Controls.Add(Me.NumericUpDown_OrdnerTiefe)
        Me.GroupBox4.Controls.Add(Me.cb_marks_visible)
        Me.GroupBox4.Location = New System.Drawing.Point(9, 568)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(352, 48)
        Me.GroupBox4.TabIndex = 15
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Optionen"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(199, 19)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(63, 13)
        Me.Label8.TabIndex = 6
        Me.Label8.Text = "OrdnerTiefe"
        '
        'NumericUpDown_OrdnerTiefe
        '
        Me.NumericUpDown_OrdnerTiefe.Location = New System.Drawing.Point(162, 16)
        Me.NumericUpDown_OrdnerTiefe.Maximum = New Decimal(New Integer() {3, 0, 0, 0})
        Me.NumericUpDown_OrdnerTiefe.Name = "NumericUpDown_OrdnerTiefe"
        Me.NumericUpDown_OrdnerTiefe.Size = New System.Drawing.Size(31, 20)
        Me.NumericUpDown_OrdnerTiefe.TabIndex = 2
        '
        'cb_marks_visible
        '
        Me.cb_marks_visible.AutoSize = True
        Me.cb_marks_visible.Checked = True
        Me.cb_marks_visible.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cb_marks_visible.Location = New System.Drawing.Point(11, 19)
        Me.cb_marks_visible.Name = "cb_marks_visible"
        Me.cb_marks_visible.Size = New System.Drawing.Size(121, 17)
        Me.cb_marks_visible.TabIndex = 0
        Me.cb_marks_visible.Text = "Zeige Markierungen"
        Me.cb_marks_visible.UseVisualStyleBackColor = True
        '
        'GB_Ort
        '
        Me.GB_Ort.Controls.Add(Me.tb_Place_Description)
        Me.GB_Ort.Controls.Add(Me.Label9)
        Me.GB_Ort.Controls.Add(Me.tb_Place_Name)
        Me.GB_Ort.Controls.Add(Me.Label4)
        Me.GB_Ort.Controls.Add(Me.Label5)
        Me.GB_Ort.Controls.Add(Me.Label6)
        Me.GB_Ort.Controls.Add(Me.tb_Place_GPS)
        Me.GB_Ort.Controls.Add(Me.tb_Place_Addresse)
        Me.GB_Ort.Location = New System.Drawing.Point(7, 171)
        Me.GB_Ort.Name = "GB_Ort"
        Me.GB_Ort.Size = New System.Drawing.Size(352, 199)
        Me.GB_Ort.TabIndex = 13
        Me.GB_Ort.TabStop = False
        Me.GB_Ort.Text = "Aufnahme Ort"
        '
        'tb_Place_Description
        '
        Me.tb_Place_Description.Location = New System.Drawing.Point(83, 97)
        Me.tb_Place_Description.Multiline = True
        Me.tb_Place_Description.Name = "tb_Place_Description"
        Me.tb_Place_Description.Size = New System.Drawing.Size(263, 96)
        Me.tb_Place_Description.TabIndex = 12
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 19)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(38, 13)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "Name:"
        '
        'tb_Place_Name
        '
        Me.tb_Place_Name.Location = New System.Drawing.Point(83, 16)
        Me.tb_Place_Name.Name = "tb_Place_Name"
        Me.tb_Place_Name.Size = New System.Drawing.Size(263, 20)
        Me.tb_Place_Name.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 100)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(69, 13)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Kommentare:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 74)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(67, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Koordinaten:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 45)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(48, 13)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Adresse:"
        '
        'tb_Place_GPS
        '
        Me.tb_Place_GPS.Location = New System.Drawing.Point(83, 71)
        Me.tb_Place_GPS.Name = "tb_Place_GPS"
        Me.tb_Place_GPS.Size = New System.Drawing.Size(263, 20)
        Me.tb_Place_GPS.TabIndex = 2
        '
        'tb_Place_Addresse
        '
        Me.tb_Place_Addresse.Location = New System.Drawing.Point(83, 42)
        Me.tb_Place_Addresse.Name = "tb_Place_Addresse"
        Me.tb_Place_Addresse.Size = New System.Drawing.Size(263, 20)
        Me.tb_Place_Addresse.TabIndex = 1
        '
        'GB_Mark
        '
        Me.GB_Mark.Controls.Add(Me.tb_AktMark_Description)
        Me.GB_Mark.Controls.Add(Me.Label11)
        Me.GB_Mark.Controls.Add(Me.tb_AktMark_Name)
        Me.GB_Mark.Controls.Add(Me.Label10)
        Me.GB_Mark.Location = New System.Drawing.Point(9, 392)
        Me.GB_Mark.Name = "GB_Mark"
        Me.GB_Mark.Size = New System.Drawing.Size(352, 150)
        Me.GB_Mark.TabIndex = 14
        Me.GB_Mark.TabStop = False
        Me.GB_Mark.Text = "Ausgewählte Markierung"
        '
        'tb_AktMark_Description
        '
        Me.tb_AktMark_Description.Location = New System.Drawing.Point(83, 45)
        Me.tb_AktMark_Description.Multiline = True
        Me.tb_AktMark_Description.Name = "tb_AktMark_Description"
        Me.tb_AktMark_Description.Size = New System.Drawing.Size(263, 99)
        Me.tb_AktMark_Description.TabIndex = 12
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(8, 22)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(38, 13)
        Me.Label11.TabIndex = 10
        Me.Label11.Text = "Name:"
        '
        'tb_AktMark_Name
        '
        Me.tb_AktMark_Name.Location = New System.Drawing.Point(83, 19)
        Me.tb_AktMark_Name.Name = "tb_AktMark_Name"
        Me.tb_AktMark_Name.Size = New System.Drawing.Size(263, 20)
        Me.tb_AktMark_Name.TabIndex = 9
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(8, 48)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(69, 13)
        Me.Label10.TabIndex = 7
        Me.Label10.Text = "Kommentare:"
        '
        'GB_Bild
        '
        Me.GB_Bild.Controls.Add(Me.tb_Bild_Description)
        Me.GB_Bild.Controls.Add(Me.Label3)
        Me.GB_Bild.Controls.Add(Me.Label2)
        Me.GB_Bild.Controls.Add(Me.Label1)
        Me.GB_Bild.Controls.Add(Me.tb_Bild_Time)
        Me.GB_Bild.Controls.Add(Me.tb_Bild_Name)
        Me.GB_Bild.Location = New System.Drawing.Point(9, 2)
        Me.GB_Bild.Name = "GB_Bild"
        Me.GB_Bild.Size = New System.Drawing.Size(352, 151)
        Me.GB_Bild.TabIndex = 12
        Me.GB_Bild.TabStop = False
        Me.GB_Bild.Text = "Bild"
        '
        'tb_Bild_Description
        '
        Me.tb_Bild_Description.Location = New System.Drawing.Point(83, 70)
        Me.tb_Bild_Description.Multiline = True
        Me.tb_Bild_Description.Name = "tb_Bild_Description"
        Me.tb_Bild_Description.Size = New System.Drawing.Size(263, 75)
        Me.tb_Bild_Description.TabIndex = 11
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 74)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(69, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Kommentare:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Zeit/-raum:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(30, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Titel:"
        '
        'tb_Bild_Time
        '
        Me.tb_Bild_Time.Location = New System.Drawing.Point(83, 45)
        Me.tb_Bild_Time.Name = "tb_Bild_Time"
        Me.tb_Bild_Time.Size = New System.Drawing.Size(263, 20)
        Me.tb_Bild_Time.TabIndex = 1
        '
        'tb_Bild_Name
        '
        Me.tb_Bild_Name.Location = New System.Drawing.Point(83, 19)
        Me.tb_Bild_Name.Name = "tb_Bild_Name"
        Me.tb_Bild_Name.Size = New System.Drawing.Size(263, 20)
        Me.tb_Bild_Name.TabIndex = 0
        '
        'TaggedIMG1
        '
        Me.TaggedIMG1.ActivMark = False
        Me.TaggedIMG1.ActivMarkDescription = ""
        Me.TaggedIMG1.ActivMarkHeight = -1
        Me.TaggedIMG1.ActivMarkLeft = -1
        Me.TaggedIMG1.ActivMarkName = ""
        Me.TaggedIMG1.ActivMarkTop = -1
        Me.TaggedIMG1.ActivMarkWidth = -1
        Me.TaggedIMG1.Cursor = System.Windows.Forms.Cursors.Default
        Me.TaggedIMG1.Description = Nothing
        Me.TaggedIMG1.ID = Nothing
        Me.TaggedIMG1.Location = New System.Drawing.Point(12, 12)
        Me.TaggedIMG1.Marks_Visible = True
        Me.TaggedIMG1.Name = "TaggedIMG1"
        Place1.Adress = Nothing
        Place1.Description = Nothing
        Place1.GPS = Nothing
        Place1.Title = Nothing
        Me.TaggedIMG1.Place = Place1
        Me.TaggedIMG1.RO = False
        Me.TaggedIMG1.Size = New System.Drawing.Size(717, 616)
        Me.TaggedIMG1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.TaggedIMG1.TabIndex = 0
        Me.TaggedIMG1.TabStop = False
        Me.TaggedIMG1.Time = Nothing
        Me.TaggedIMG1.Title = Nothing
        '
        'SB_PictureTagger
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1106, 723)
        Me.Controls.Add(Me.TaggedIMG1)
        Me.Controls.Add(Me.Panel_Right)
        Me.Controls.Add(Me.Panel_Botom)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(1122, 761)
        Me.Name = "SB_PictureTagger"
        Me.Text = "SB-PictureTagger"
        Me.Panel_Botom.ResumeLayout(False)
        Me.Panel_Botom.PerformLayout()
        Me.Panel_Right.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.NumericUpDown_OrdnerTiefe, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GB_Ort.ResumeLayout(False)
        Me.GB_Ort.PerformLayout()
        Me.GB_Mark.ResumeLayout(False)
        Me.GB_Mark.PerformLayout()
        Me.GB_Bild.ResumeLayout(False)
        Me.GB_Bild.PerformLayout()
        CType(Me.TaggedIMG1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel_Botom As System.Windows.Forms.Panel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents b_reloaddir As System.Windows.Forms.Button
    Friend WithEvents b_loaddir As System.Windows.Forms.Button
    Friend WithEvents b_exit As System.Windows.Forms.Button
    Friend WithEvents b_last As System.Windows.Forms.Button
    Friend WithEvents b_next As System.Windows.Forms.Button
    Friend WithEvents Panel_Right As System.Windows.Forms.Panel
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown_OrdnerTiefe As System.Windows.Forms.NumericUpDown
    Friend WithEvents cb_marks_visible As System.Windows.Forms.CheckBox
    Friend WithEvents GB_Ort As System.Windows.Forms.GroupBox
    Friend WithEvents tb_Place_Description As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents tb_Place_Name As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tb_Place_GPS As System.Windows.Forms.TextBox
    Friend WithEvents tb_Place_Addresse As System.Windows.Forms.TextBox
    Friend WithEvents GB_Mark As System.Windows.Forms.GroupBox
    Friend WithEvents tb_AktMark_Description As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents tb_AktMark_Name As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents GB_Bild As System.Windows.Forms.GroupBox
    Friend WithEvents tb_Bild_Description As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tb_Bild_Time As System.Windows.Forms.TextBox
    Friend WithEvents tb_Bild_Name As System.Windows.Forms.TextBox
    Friend WithEvents TaggedIMG1 As TaggedIMG

End Class
